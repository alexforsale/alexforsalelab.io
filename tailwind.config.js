/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: 'class',
	content: [
		"*.css",
		"./content/js/*.js",
		"./node_modules/flowbite/**/*.js",
		"./public/**/*.{css,js,html}",
	],
	theme: {
		extend: {},
	},
	plugins: [
		require('flowbite/plugin'),
		require('@tailwindcss/typography'),
		require('tailwindcss-opentype'),
	],
}
