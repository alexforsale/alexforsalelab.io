.PHONY: all assets css js org publish

PUBLISH_DIR ?= "public"
all: publish

assets:
	@cp -rv node_modules/@fortawesome/fontawesome-free/ $(PUBLISH_DIR)

css:
	@echo "Compiling tailwind css files..."
	npx tailwind -i input.css -o contents/css/output.css

js:
	@mkdir -pv $(PUBLISH_DIR)/js
	@cp -v ./node_modules/jquery/dist/jquery.min.* $(PUBLISH_DIR)/js/
	@cp -v ./node_modules/jquery-ui/dist/jquery-ui.min.* $(PUBLISH_DIR)/js/
	@cp -v ./node_modules/jquery.localscroll/jquery.localScroll.min.* $(PUBLISH_DIR)/js/
	@cp -v ./node_modules/jquery.scrollto/jquery.scrollTo.min.* $(PUBLISH_DIR)/js/

org:
	@echo "Publishing org files..."
	emacs -Q --batch --eval "(setq +config/publish-dir \"$(PUBLISH_DIR)\")" --load publish.el --funcall org-publish-all

publish:
	make assets js org css

clean:
	@echo "Cleaning up..."
	@rm -rfv *.elc
	@rm -rvf $(PUBLISH_DIR)/*
	@rm -rfv .org-timestamps/*
