;;; publish --- Separate publishing configuration file -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
(require 'ox-publish)

(customize-set-variable 'org-publish-timestamp-directory (expand-file-name ".org-timestamps/") default-directory)
(unless (file-directory-p org-publish-timestamp-directory)
  (make-directory org-publish-timestamp-directory t))
(customize-set-variable 'package-user-dir (expand-file-name ".emacs-packages") default-directory)
(unless (file-directory-p package-user-dir)
  (make-directory package-user-dir t))
(customize-set-variable 'org-download-image-dir (expand-file-name "contents/img" default-directory))

;;; `org-mode' configuration
(setq org-todo-keywords
   '((sequence
      "TODO(t!)"  ; A task that needs doing & is ready to do
      "NEXT(n!)"  ; Tasks that can be delayed
      "PROJ(p!)"  ; A project, which usually contains other tasks
      "PROG(g!)"  ; A task that is in progress
      "WAIT(w!)"  ; Something external is holding up this task
      "HOLD(h!)"  ; This task is paused/on hold because of me
      "IDEA(i!)"  ; An unconfirmed and unapproved task or notion
      "|"
      "DONE(d!)"  ; Task successfully completed
      "DELEGATED(l!)" ; Task is delegated
      "KILL(k!)") ; Task was cancelled, aborted or is no longer applicable
     )
   org-tag-alist '(("work" . ?w)
                   ("followup" . ?f)
                   ("project" . ?p)
                   ("personal" . ?h)
                   ("reading" . ?r)
                   ("links" . ?l))
   org-todo-keyword-faces
   '(("PROJ" . (:foreground "cyan" :weight bold))
     ("WAIT" . (:foreground "yellow" :weight bold))
	 ("IDEA" . (:foreground "magenta" :weight bold))
     ("DELEGATED" . "blue")
     ("KILL" . "green")))

(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'htmlize)
  (package-refresh-contents)
  (package-install 'htmlize))

(unless (package-installed-p 'rustic)
  (package-refresh-contents)
  (package-install 'rustic))

(unless (package-installed-p 'flycheck)
  (package-refresh-contents)
  (package-install 'flycheck))

(unless (package-installed-p 'lsp-mode)
  (package-refresh-contents)
  (package-install 'lsp-mode))

(unless (package-installed-p 'org-download)
  (package-refresh-contents)
  (package-install 'org-download))

(require 'htmlize)
(require 'rustic)
(require 'org-download)
(setq org-download-method 'directory
      org-download-link-format "[[../img/%s]]\n")
(require 'org-attach)
(setq org-attach-auto-tag nil)

(defun +config/org-publish-org-sitemap (title list)
  "Sitemap generation function."
  (concat (format "#+TITLE: %s\n\n* Latest Posts\n\n" title)
          (org-list-to-subtree list 2)))

(defun +config/org-publish-org-sitemap-format-entry (entry style project)
  (cond ((not (directory-name-p entry))
         (let* ((date (org-publish-find-date entry project)))
           (format "%s - [[file:%s][%s]]"
                   (format-time-string "%F %R" date) entry
                   (org-publish-find-title entry project))))
        ((eq style 'tree)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(when (getenv "GITLAB_USER_NAME")
  (setq user-login-name (getenv "GITLAB_USER_NAME"))
  (setq user-full-name (getenv "GITLAB_USER_NAME")))
(when (getenv "GITLAB_USER_EMAIL")
  (setq user-mail-address (getenv "GITLAB_USER_EMAIL")))
(setq make-backup-files nil)
(setq org-confirm-babel-evaluate nil)
(setq org-html-htmlize-output-type 'css)
(setq user-full-name user-login-name)
(setq user-mail-address "alexforsale@yahoo.com")
(setq org-use-sub-superscripts '{})

(defvar +config/base-url (or (when (getenv "BASE_URL") (getenv "BASE_URL")) "/")
  "Base URL.")

(defvar +config/publish-dir (or (getenv "PUBLISH_DIR")
								(expand-file-name
								 "public/"
								 (file-name-as-directory
								  (file-name-directory
								   (or load-file-name buffer-file-name)))))
  "Initial blog directory.")

(defun +config/html-head-extra ()
  "Extra html head."
  (concat
   "<link type=\"text/css\" href=\"/css/output.css\" rel=\"stylesheet\">\n"
   "<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\">\n"
   "<script src=\"/js/jquery.min.js\"></script>"
   "<script type=\"text/javascript\" src=\"/js/jquery.min.js\"></script>"
   "<script type=\"text/javascript\" src=\"/js/jquery-ui.min.js\"></script>"
   "<script type=\"text/javascript\" src=\"/js/jquery.localScroll.min.js\"></script>"
   "<script type=\"text/javascript\" src=\"/js/jquery.scrollTo.min.js\"></script>"
   "<script type=\"text/javascript\" defer src=\"/fontawesome-free/js/brands.js\"></script>"
   "<script type=\"text/javascript\" defer src=\"/fontawesome-free/js/solid.js\"></script>"
   "<script type=\"text/javascript\" defer src=\"/fontawesome-free/js/fontawesome.js\"></script>"
   "<script>
  // It's best to inline this in `head` to avoid FOUC (flash of unstyled content) when changing pages or themes
  if (
    localStorage.getItem('color-theme') === 'dark' ||
    (!('color-theme' in localStorage) &&
      window.matchMedia('(prefers-color-scheme: dark)').matches)
  ) {
    document.documentElement.classList.add('dark');
  } else {
    document.documentElement.classList.remove('dark');
  }</script>"
   ))

(defun +config/org-export-format-drawer (name content)
  "Format NAME and CONTENT using html syntax."
  (concat "<div class=\"drawer " (downcase name) "\">\n"
          "<h6>" (capitalize name) "</h6>\n"
          content
          "\n</div>"))

(setq org-publish-project-alist
	  `(("org-notes"
		 :base-directory "contents"
		 :base-extension "org"
		 :recursive t
		 :exclude "rss\\.org\\|sitemap\\.org"
		 :publishing-directory ,+config/publish-dir
		 :publishing-function org-html-publish-to-html
		 :headline-levels 5
		 :section-numbers nil
		 :with-planning t
		 :with-special-strings t
		 :with-sub-superscript nil
		 :with-toc nil
		 :with-tables t
		 :with-tags t
		 :with-tasks t
		 :with-timestamps t
		 :with-title t
		 :with-drawers t
		 :with-properties t
		 :with-clocks t
		 :with-todo-keywords t
		 :with-email t
		 :with-author t
		 :with-date t
		 :with-emphasize t
		 :with-priority t
		 :with-special-strings t
		 :with-footnotes t
		 :html-extension "html"
		 :html-doctype "html5"
		 :html-html5-fancy t
		 :auto-sitemap t
		 :with-tables t
		 :sitemap-filename "sitemap.org"
		 :sitemap-function +config/org-publish-org-sitemap
		 :sitemap-format-entry +config/org-publish-org-sitemap-format-entry
		 :sitemap-title "alexforsale"
		 :sitemap-sort-files anti-chronologically
		 :html-preamble t
		 :html-preamble-format (("en" "
<nav id=\"nav\">
  <div id=\"main-link\">
    <ul>
      <li>
        <a href=\"/\">Home</a>
      </li>
      <li>
        <a href=\"/notes\">Notes</a>
      </li>
      <li>
        <a href=\"/contact\">Contact</a>
      </li>
      <li>
        <a href=\"/about\">About</a>
      </li>
      <li>
        <div id=\"theme-toggle\">
          <button id=\"theme-toggle-dark-icon\" type=\"button\" class=\"hidden\"><i class=\"fa-solid fa-moon\"></i></button>
          <button id=\"theme-toggle-light-icon\" type=\"button\" class=\"hidden\"><i class=\"fa-solid fa-sun\"></i></button>
        </button>
      </li>
    </ul>
  </div>
</nav>
"))
		 :html-postamble t
		 :html-postamble-format (("en" "
<footer id=\"footer\">
  <div id=\"leftside\">
    <span>© %C - <a target=\"_blank\"  href=\"mailto:alexforsale@alexforsale.site\">%a</a></span>
    <!-- <p class=\"text-xs\"><a class=\"hover:underline\" href=\"https://github.com/alexforsale\">github</a> - <a class=\"hover:underline\" href=\"https://gitlab.com/alexforsale\">gitlab</a class=\"hover:underline\" > - <a href=\"https://linkedin.com/in/alexforsale\">linkedin</a></p> -->
    <ul class=\"text-xs\">
      <li>
        <a class=\"hover:underline\" href=\"/notes\">Notes</a>
      </li>
      <li>
        <a class=\"hover:underline\" href=\"/contact\">Contact</a>
      </li>
      <li>
        <a class=\"hover:underline\" href=\"/about\">About</a>
      </li>
    </ul>
  </div>
  <div id=\"bottom\">
    <p class=\"text-xs\">Made using %c. Last exported <span class=\"timestamp-wrapper\">%T</span>. Contents are licensed under the <a target=\"_blank\"  class=\"hover:underline\" href=\"https://opensource.org/license/mit/\">MIT License</a> or <a class=\"hover:underline\" target=\"_blank\" href=\"https://opensource.org/license/gpl-3-0/\">GPLv3</a> unless otherwise indicated. Please feel free to reuse or share stuff under a <a class=\"hover:underline\" target=\"_blank\" \"href=\"https://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution</a> license unless otherwise noted.</p>
    <p class=\"text-xs\">%v</p>
  </div>
<script type=\"text/javascript\" src=\"/js/hideshow.js\"></script>
<script type=\"text/javascript\" src=\"/js/blog.js\"></script>
"))
		 :html-footnote-format "<sup>%s</sup>"
		 :html-footnote-separator "<sup>, </sup>"
		 :html-footnotes-section "<div id=\"footnotes\" class=\"block\">\n<div><h4>%s: </h4>\n%s\n</div>\n</div>"

		 :html-head nil
		 :html-head-extra ,(+config/html-head-extra)
		 :html-format-drawer-function +config/org-export-format-drawer
		 :html-viewport ((width "device-width")
						 (initial-scale "1.0")
						 (minimum-scale "")
						 (maximum-scale "")
						 (user-scalable ""))
		 ;; :html-preamble t
		 ;; :html-preamble-format
		 :html-head-include-default-style nil
		 ;;:html-checkbox-type 'ascii
		 :html-self-link-headlines t
		 :html-validation-link "
<a href=\"https://validator.w3.org/check?uri=referer\" class=\"inline-flex items-center mt-4 mb-1 p-1 text-sm font-medium text-center rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 bg-blue-700 focus:bg-blue-600 hover:bg-blue-800\" target=\"_blank\">Validate</a>"
		 :html-wrap-src-lines t)
		("org-static"
		 :base-directory "contents"
		 :base-extension "ico\\|css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
		 :publishing-directory ,+config/publish-dir
		 :recursive t
		 :publishing-function org-publish-attachment)
		("org"
		 :components ("org-notes" "org-static"))))

(provide 'publish)
;;; publish.el ends here

